<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use App\Sim;
class SimSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Sim');
       



        for ($i=0; $i <= 200 ; $i++) { 
            $network = array("airtel", "mtn","etisalat", "glo");
            $type = array("regular", "special");
            $number = '020';
            $newtorkIndex = array_rand($network);
            $typeIndex = array_rand($type);
            if($type[$typeIndex] === 'special'){
                $number = '030';
            }
            
    		DB::table('sims')->insert([
    		    'type' => $type[$typeIndex],
                'network' => $network[$newtorkIndex],
    			'number' => $number.$faker->randomNumber(8),
    			

    		]);
    	}
    }
}
