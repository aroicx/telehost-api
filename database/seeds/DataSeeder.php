<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use App\Data;
class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\Data');
       



        for ($i=0; $i <= 50 ; $i++) { 
            $network = array("airtel", "mtn","9mobile", "glo");
            $type = array("regular", "special");
            $number = '020';
            $newtorkIndex = array_rand($network);
            $typeIndex = array_rand($type);
            if($type[$typeIndex] === 'special'){
                $number = '030';
            }
            
    		DB::table('data')->insert([
    		    'name' => '10Mb',
                'amount' => 25,
                'network' => $network[$newtorkIndex],
                'bundleCode' => '00'.$i,
                'code' => '100'.$i,
                'qty' => 1

    		]);
    	}
    }
}
