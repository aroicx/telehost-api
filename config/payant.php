<?php

return [

    /**
     * Public Key From Payant Dashboard
     *
     */
    'public_key' => env('PAYANT_PUBLIC_KEY'),

    /**
     * Private Key From Payant Dashboard
     *
     */
    'private_key' => env('PAYANT_PRIVATE_KEY'),

    /**
     * Payant API MODE
     */
    'mode' => env('PAYANT_MODE'),
];