<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\Duration;
use App\Devices;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       
        $schedule->call(function () {
            $devices = Devices::all();

            $expired = [];
     
            foreach ($devices as $key => $value) {
             $date = Carbon::parse($value->updated_at);
             $now = Carbon::now();
             $diff = $date->diffInDays($now);
             $failed = ['user' => $value->user_id, 'duration' => $value->duration, 'expired_time' => $diff];
     
             array_push($expired,$failed);
             // sleep(6);
            }
     
     
            $proven = [];
     
            foreach ($expired as $key => $value) {
             // echo $value['duration'];
     
               if($value['expired_time'] > $value['duration']){
                 $proof = ['user' => $value['user'], 'expired' => true];
               }else{
                 $proof = ['user' => $value['user'], 'expired' => false];
               }
               array_push($proven,$proof);
     
            }
     
            foreach ($proven as $key => $value) {
     
            
                 if($value['expired']){
                      Devices::where('user_id', $value['user'])->update(['status' => 0]);
                     // echo $value['user'];
                 }
            }
        })->everyMinute();

        echo 'Done';
    }



    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
