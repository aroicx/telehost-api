<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Devices;

class Duration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'duration:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Duration toggle status based on if the duration limit is expired or not';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       Devices::where('user_id',1)->update(['status' => 0]);
       echo 'Done';
    }
}
