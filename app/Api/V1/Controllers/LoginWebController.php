<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\User;
use App\Wallet;
use App\Devices;


class LoginWebController extends Controller
{
    /**
     * Log the user in
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {

        $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL ) 
        ? 'email' 
        : 'phone_num';

        $request->merge([
            $login_type => $request->login
        ]);

        $credentials = $request->only([$login_type, 'password']);

        try {
            $token = Auth::guard()->attempt($credentials);

            if(!$token) {
                throw new AccessDeniedHttpException();
            }

        } catch (JWTException $e) {
            throw new HttpException(500);
        }
        $user = Auth::User()->id;
        $wallet = Wallet::where('user_id',$user)->first();
        // $device = Devices::where('user_id',$user)->first();
        if($wallet){
            $details = array(
                Auth::User(),
                'wallet_balance' => $wallet->balance
            );
        }else{
            $wallet = new Wallet;
            $wallet->user_id = $user;
            $wallet->balance = '0.00';
            $wallet->save();
            $details = array(
                Auth::User(),
                'wallet_balance' => '0.00'
            );
        }  

        // if(!$device){
        //     $dev = new Devices;
        //     $dev->user_id = $user;
        //     $dev->save();
        // }
        


        return response()
            ->json([
                'status' => 'ok',
                'token' => $token,
                'user' => $details,
                'expires_in' => 30
            ]);
    }
}
