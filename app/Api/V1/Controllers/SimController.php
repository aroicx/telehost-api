<?php

namespace App\Api\V1\Controllers;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\Sim;

class SimController extends Controller
{


    public function all()
    {
        $regular = Sim::where('type','regular')->where('assigned',0)->paginate(20);
        $special = Sim::where('type','special')->where('assigned',0)->paginate(20);

        return response()->json([
            'status' => '200',
            'regular' => $regular,
            'special' => $special
        ]);



        
    }
}
 