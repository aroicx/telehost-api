<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Data;
use App\Ussd;
use App\Wallet;
use App\Devices;
use App\Transaction;


class DataController extends Controller
{
    

    public function __construct(){
    $headers = getallheaders();

        $verify = User::where('api_key',$headers['Authorization'])->first();
        if(!$verify){
            
            return response()->json([
                'status' => 'error',
                'msg' => 'Invaild Api Key'
                
            ]);
            
        }



    }

    public function getNetwork(Request $request,$network)
    {

        $headers = getallheaders();

        $verify = User::where('api_key',$headers['Authorization'])->first();
        if(!$verify){
            
            return response()->json([
                'status' => 'error',
                'msg' => 'Invaild Api Key'
                
            ]);
            
        }

        $data = Data::where('network',$network)->get();


        return response()->json([
            'status' => 'success',
            'data' => $data
            
        ]);




    }


    public function buyData(Request $request)
    {

      $headers = getallheaders();

      $verify = User::where('api_key',$headers['Authorization'])->first();
      if($verify){
        $request->validate([
          'ref_code' => 'required|unique:ussds'
      ]);
      $this->validateParameter('ref_code', $request->ref_code, STRING);
      $this->validateParameter('number', $request->number, STRING);
      $this->validateParameter('bundle_code', $request->bundle_code, STRING);
      $this->validateParameter('access_code', $request->access_code, STRING);

      $user = $verify->id;
      $getToken =  Devices::where('user_id',$user)->where('access_code', $request->access_code)->first();
      if(!$getToken){
        return response()->json([
          'status' => 'error',
          'msg' => 'Access Code error'
        
        ]);
      }
        if($getToken->status != 1){

          return response()->json([
            'status' =>  'error',
            'msg' => 'Current Devices activations is expired'
          ]);

        }else{
            $getUssd = Ussd::where('ref_code',$request->ref_code)->first();
            $getBundle = Data::where('bundleCode',$request->bundle_code)->first();
            if(!$getBundle){
                return response()->json([
                    'status' =>  'error',
                    'msg' => 'Invaild Bundle Code'
                  ]);
            }
            $toDail = '*'.$getBundle->ussd.'*'.$request->number.'#';
            // return $toDail;

            
            if(!$getUssd){
              $ussd = new Ussd;
              $ussd->user_id = $verify->id;
              $ussd->ref_code = $request->ref_code;
              $ussd->code_dialed = $toDail;
              $ussd->access_code = $request->access_code;
              $ussd->save();
            }
           

          
            $data = [  
              
                "to" => $getToken->device_token,
                "collapse_key" =>"aKey",
                "data" => [
                  "ref_code" => $request->ref_code,
                  "ussdString" => $toDail,
                  "sim" => $getToken->sim_port,
                ],
                "time_to_live" => 30,
              
            ];

          
            $toPost = json_encode($data);
        

            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $toPost,
              CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Authorization: key=AAAA0Ft6vlE:APA91bFATBXrgoQ3NinRAO8BNB9Cd3KODh42_lc0YhjOFUyN0MOvYs2KI7EUY2yA2p9dtTdtFK6wnp8B-X07HIaIOi92KxIENVBPIEweGdUkZthYZwA1UaLhPANZDIV0BuuCk9qmiJTw",
                "Content-Type: application/json"

              ),
            ));

              $response = curl_exec($curl);
              $err = curl_error($curl);

              curl_close($curl);

              if ($err) {
                // echo "cURL Error #:" . $err;
              } else {
                $character = json_decode($response);
                if ($character->success === 1) {
                  Ussd::where('ref_code' , $request->ref_code)->update(['status' => 1]);
                 
                  return response()->json($response);
                }else{
                  return response()->json($response);
                }

                
              }


            } 
        }else {
          return response()->json([
            'status' => 'error',
            'msg' => 'Invaild Api Key'
          
          ]);
        }


    }

    public function create(Request $request)
    {
      $headers = getallheaders();
      $this->validateParameter('ussd_code', $request->ussd_code, STRING);
      $this->validateParameter('ref_code', $request->ref_code, STRING);
      $this->validateParameter('message', $request->message, STRING);

      $verify = User::where('api_key',$headers['Authorization'])->first();
      if($verify){
        // sleep(5);
        $refCode = strtolower($request->ref_code);
        $success = ResquestTransaction::where('ref_code',$refCode)->first();

        $msg = $request->message;

        if($msg === 'Sorry, the process failed' ){
            $getRef = Ussd::where('ref_code',$request->ref_code)->first();

            $data = array(
              'ref_code' => $request->ref_code,
              'ussd_code' => $request->ussd_code,
              'access_code' => $getRef->access_code,
            );
            

          $this->tryAgain($data);
          die;
        }

        $haystack = 'how are you';
        $check = 'Failed to run';



        if(strpos($msg,$check) !== false){
            $getRef = Ussd::where('ref_code',$request->ref_code)->first();

            $data = array(
              'ref_code' => $request->ref_code,
              'ussd_code' => $request->ussd_code,
              'access_code' => $getRef->access_code,
            );
            

          $this->tryAgain($data);
          die;
        }
                
        if($success){

          ResquestTransaction::where('ref_code',$request->ref_code)
                  ->update(['message' => $request->message]);
        }else{

          $rt = new ResquestTransaction;
          $rt->user_id = $verify->id;
          $rt->ref_code = $request->ref_code;
          $rt->message = $request->message;
          $rt->save();
  
        }

       
        // echo $verify->webhook;
        if($verify->webhook != null || ''){
          $this->ussd_webhook($request,$verify->webhook);
          return response()->json(['status' => 'success', 'msg' => 'sent to webhook' ]);
        }



        return response()->json(['status' => 'success' ]);

      } else {
        return response()->json([
          'status' => 'error',
          'msg' => 'Invaild Api Key'
         
        ]);
      }
     
    }




    public function data_webhook(Request $request,$url)
    {

      $data = $request->all();
      $toPost = json_encode($data);
      $curl = curl_init();

   
      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $toPost,
        CURLOPT_HTTPHEADER => array(
          "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vdGVzdC1wcm9qZWN0LnRlc3QvYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1NjQ2NTE1NTAsImV4cCI6MTU2NDY1NTE1MCwibmJmIjoxNTY0NjUxNTUwLCJqdGkiOiJYZ2wxWW9pWXBnU1hnYVgwIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.F_HgymVcC_bJyNjLyt-CT-o9_-gD28fyNNZnu73rdo4",
          "Content-Type: application/json",
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        // echo $response;
      }



    }



}
