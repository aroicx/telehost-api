<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\User;
use App\Wallet;
use App\Transaction;
use App\Devices;
use App\Ussd;
use App\Message;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */


    public function dashboard()
    {
        $user = Auth::user();

        $devices =  Devices::where('user_id' , $user->id)->get();
        $wallet = Wallet::where('user_id',$user->id)->first();
        $trans = Transaction::where('user_id',$user->id)->get();
        $getTrans = Transaction::where('user_id',$user->id)->paginate(5);
        $device = Devices::where('user_id',$user->id)->get();
        $ussd = Ussd::where('user_id',$user->id)->get();
        $message = Message::where('user_id',$user->id)->get();
    
        return response()->json(['status' => 'success','user' => Auth::User(),
        'wallet' => $wallet->balance,
        'transactions' => count($trans),
        'devices' => count($device),
        'ussd' => count($ussd),
        'sms' => count($message),
        'recent_trans' => $getTrans
        ]);
    }

    public function me()
    {
        $user = Auth::User()->id;
        $wallet = Wallet::where('user_id',$user)->first();
        $trans = Transaction::where('user_id',$user)->get();
        $device = Devices::where('user_id',$user)->get();
       
        $details = array(
            'user' => Auth::User(),
            'wallet' => $wallet,
            'transactions' => $trans,
            'devices' => $device
        );
       
        return response()->json($details);
    }
    public function check()
    {
        $user = Auth::User()->id;
        $check = User::where('id',$user)->first();
        

        if ($check->is_admin === 0) {
            return response()->json([
                'status' => 'success',
                'message' => false
            ]);
        }else{
            return response()->json([
                'status' => 'success',
                'message' => true
            ]);
        }
       
      
    }

    public function addWebhook(Request $request)
    {
        $user = Auth::User()->id;
        User::where('id',$user)->update(['webhook' => $request->webhook]);
        return response()->json([
            'status' => 'success',
            'message' => 'Hook Added Successfully'
        ]);
        // return $request->all();
    }

    public function  generatePin($length) {
        $key = '';
        $keys = array_merge(range(0, 9));
       for ($i = 0; $i < $length; $i++) {
                $key .= $keys[array_rand($keys)];
         }
    }

    // public function mail()
    // {
    //     try {
    //         $code = $this->generatePin(6);
    //         Mail::to(Auth::User()->email)->send(new UserActivation(Auth::User(),$code));
    //         return response()->json(['status' => '201','message' => 'Mail Sent']);
    //     } catch (JWTException $e) {
    //         throw new HttpException(500);
    //     }
    
    // }

    public function verify($token)
    {
        try {
            $details = User::where('api_key',$token)->where('verified', 0)->get();
          
            if(count($details) > 0){
                User::where('api_key',$token)->update(['verified' => 1]);
                return response()->json(['status' => '400','message' => $details[0]->firstname.' '.$details[0]->lastname.' has been verified']);
            }else{
                return response()->json(['status' => '400','message' => 'Link Expired' ]);  
            }
        } catch (JWTException $e) {
            throw new HttpException(500);
        }
      
       
    }


    public function updatePassword(Request $request)
    {

        $this->validateParameter('password', $request->password, STRING);
        $this->validateParameter('newpassword', $request->newpassword, STRING);
        $this->validateParameter('confirmpassword', $request->confirmpassword, STRING);
       


        $id = Auth::user()->id;
        $oldpassword = $request->password;
        $newpassword = $request->newpassword;
        $confirmPassword = $request->confirmPassword;
        if (User::where('id', $id)){
            if (Hash::check($oldpassword, Auth::user()->password, [])) {
                
                    $data = array(
                        'password' => bcrypt($newpassword),
                    );

                    User::where('id', $id)->update($data);

                    $notification = array(

                        'status' => 'success', 
                        'message' => 'Successful... You Have Channged your Password !'
                    );
                    return response()->json($notification);
                // }else{

                //     $notification = array(

                //         'statu' => 'error', 
                //         'message' => 'New Password & Confirm Password No Match '
                //     );
                //     return response()->json($notification);
                // }
            }else{

                $notification = array(

                    'status' => 'error', 
                    'message' => 'Old Password is invaild '
                );
                return response()->json($notification);
            }
        }
    }

    
}

