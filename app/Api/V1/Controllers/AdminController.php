<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Http\Controllers\Controller;
use App\Api\V1\Services\Dedicated;
use App\Api\V1\Services\Shared;
use Auth;
use App\User;
use App\Wallet;
use App\Devices;
use App\Sim;
use App\Transaction;
use PayantNG\Payant;
use App\Mail\WalletDebited;

//use carbon
use Carbon\Carbon;



class AdminController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('admin', []);
    // }

    public function getUsers()
    {
        $users = User::all();
        return response()->json(['status' => 'success', 'data' => $users]);
    }

    public function getUser($id)
    {
        $user = User::where('id',$id)->first();
        $wallet = Wallet::where('user_id',$user->id)->first();
        $transaction = Transaction::where('user_id',$user->id)->get();
        return response()->json([
            'status' => 'success',
            'user' => $user,
            'wallet' => $wallet,
            'transactions' => $transaction
        ]);
    }

    public function getWallets()
    {
        
        $wallets = Wallet::all();
        // $parent = [];
        // foreach ($wallets as $key => $value) {
        //     $id = $value->user_id;
        //     $user = User::where('id', $id)->first();
        //     array_push($parent,$user);
        // }

        // echo $parent;
        // $details = array_push($wallets,$parent);
       
        return response()->json($wallets);
    }
    public function getTransactions()
    {
        $trans = Transaction::all();
        return response()->json($trans);
    }
    

    public function DurationCron()
    {
       $devices = Devices::all();

       $expired = [];

       foreach ($devices as $key => $value) {
        $date = Carbon::parse($value->updated_at);
        $now = Carbon::now();
        $diff = $date->diffInDays($now);
        $failed = ['user' => $value->user_id, 'duration' => $value->duration, 'expired_time' => $diff];

        array_push($expired,$failed);
        // sleep(6);
       }


       $proven = [];

       foreach ($expired as $key => $value) {
        // echo $value['duration'];

          if($value['expired_time'] > $value['duration']){
            $proof = ['user' => $value['user'], 'expired' => true];
          }else{
            $proof = ['user' => $value['user'], 'expired' => false];
          }
          array_push($proven,$proof);

       }

       foreach ($proven as $key => $value) {

       
            if($value['expired']){
                //  Devices::where('user_id', $value['user'])->update(['status' => 0]);
                echo $value['user'];
            }
       }





      


       return response()->json($proven);
    }


    public function schedule()
    {
        $devices = Devices::all();

        $expired = [];
 
        foreach ($devices as $key => $value) {
         $date = Carbon::parse($value->updated_at);
         $now = Carbon::now();
         $diff = $date->diffInDays($now);
         $failed = ['user' => $value->user_id, 'duration' => $value->duration, 'expired_time' => $diff];
 
         array_push($expired,$failed);
         // sleep(6);
        }
      
 
 
        $proven = [];
 
        foreach ($expired as $key => $value) {
         // echo $value['duration'];
 
           if($value['expired_time'] > $value['duration']){
             $proof = ['user' => $value['user'], 'expired' => true];
           }else{
             $proof = ['user' => $value['user'], 'expired' => false];
           }
           array_push($proven,$proof);
 
        }

        // return response()->json($proven);
 
        foreach ($proven as $key => $value) {
 
        
             if($value['expired']){
                  Devices::where('user_id', $value['user'])->update(['status' => 0]);
                 echo $value['user'];
             }
        }
       
    }



   


}
