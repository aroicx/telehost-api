<?php
namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Wallet;
use App\Devices;
use App\Transaction;


class DevicesController extends Controller
{

     public function index()
    {
       $user = Auth::user();

      $devices =  Devices::where('user_id' , $user->id)->get();

      return response()->json([
          'status' => 'success',
          'data' => $devices
      ]);
    }
    public function transactionHelper($user,$amount,$choice)
    {
        $choosen = null;
        switch ($choice) {
            case 'topup':
            $choosen = 'topup';
                break;
            
            case 'activation':
            $choosen = 'activation';
                break;

            case 'sms':
            $choosen = 'sms';
                break;
            
            case 'ussd':
            $choosen = 'ussd';
                break;

            case 'call':
            $choosen = 'call';
                break;

            case 'sim':
            $choosen = 'sim';
                break;
            
            default:
                # code...
                break;
        }

       try {
        $transaction = new Transaction;
        $transaction->user_id = $user->id;
        $transaction->transaction_type = $choosen;
        $transaction->amount = $amount;
        $transaction->save();
       } catch (JWTException $e) {
        throw new HttpException(500);
        }


    }

    public function debitHelper($user,$amount)
    {
      
       try {
            $wal = Wallet::where('user_id', $user->id)->first();
            $debited = $wal->balance - $amount;
            Wallet::where('user_id', $user->id)->update(['balance' => $debited]);
       } catch (JWTException $e) {
        throw new HttpException(500);
        }


    }
    public function checkBalance($user,$amount){

       
        $wal = Wallet::where('user_id', $user->id)->first();
        $bal = $wal->balance;
        $check = 100 < 10;

        echo $check;
    
        if($bal < $amount){
            
            $res = array('status' => 'error', 'message' => 'Insufficient fund');
            return response()->json($res);
            die;
        }
        
        
    }

    public function  generateKey($length) {
        $key = '';
        $keys = array_merge(range(0, 9),range('a', 'z'));
        for ($i = 0; $i < $length; $i++) {
                $key .= $keys[array_rand($keys)];
            }
            return $key;
    }
    public function store(Request $request)
    {
        $deciveToken = $request->device;
        $Phonenumber = $request->phone_number;

        $this->validateParameter('device', $deciveToken, STRING);
        $this->validateParameter('phone_number', $Phonenumber, STRING);

        $device = new Devices;
        $device->user_id = Auth::user()->id;
        $device->device_token = $deciveToken;
        $device->number = $Phonenumber;
        $device->save();


        return response()->json([
            'status' => 'ok',
            'msg' => 'Successfully Stored'
        ]);





    }

   public function activate(Request $request)
   {

    $headers = getallheaders();

    $this->validateParameter('device_token', $request->device_token, STRING);
    $this->validateParameter('duration', $request->duration, STRING);
    $this->validateParameter('sim_port', $request->sim_port, STRING);
    $this->validateParameter('device_name', $request->device_name, STRING);


    $verify = User::where('api_key',$headers['Authorization'])->first();
    if($verify){
      
        $gotten = Devices::where('user_id',$verify->id)
                            ->where('device_name',$request->device_name)->first();

        

        if($gotten){
            // echo $gotten->status;
            // die;
            if($gotten->status === 1){
                Devices::where('user_id',$verify->id)
                ->where('device_name',$request->device_name)
                ->update(['user_id' => $verify->id,'device_token' => $request->device_token]);

                return response()->json([
                    'status' => 'success',
                    'msg' => 'Updated Successfully',
                    'data' => $gotten
                ]);
            }else{
                switch ($request->duration) {
                    case '7':
    
                    $wal = Wallet::where('user_id', $verify->id)->first();     
                    if($wal->balance < 600){
                        $res = array('status' => 'error', 'message' => 'Insufficient fund');
                        return response()->json($res);
                        die;
                    }
                   
                    $this->debitHelper($verify,'600');
                    $this->transactionHelper($verify,'600','activation');
                        break;
                    
                    case '30':
                    $wal = Wallet::where('user_id', $verify->id)->first();  
                    if($wal->balance < 2500){
                        $res = array('status' => 'error', 'message' => 'Insufficient fund');
                        return response()->json($res);
                        die;
                    }
                    
                    $this->debitHelper($verify,'2500');
                    $this->transactionHelper($verify,'2500','activation');
                        break;
                    
                    default:
                        # code...
                        break;
                }

                Devices::where('user_id',$verify->id)
                ->where('device_name',$request->device_name)
                ->update(['user_id' => $verify->id, 'duration' => $request->duration, 'device_token' => $request->device_token,'status' => 1]);

                return response()->json([
                    'status' => 'success',
                    'msg' => 'Reactivation Successfully',
                    'data' => $gotten
                ]);
    
            }
           
        }else{
           
            switch ($request->duration) {
                case '7':

                $wal = Wallet::where('user_id', $verify->id)->first();     
                if($wal->balance < 600){
                    $res = array('status' => 'error', 'message' => 'Insufficient fund');
                    return response()->json($res);
                    die;
                }
               
                $this->debitHelper($verify,'600');
                $this->transactionHelper($verify,'600','activation');
                    break;
                
                case '30':
                $wal = Wallet::where('user_id', $verify->id)->first();  
                if($wal->balance < 2500){
                    $res = array('status' => 'error', 'message' => 'Insufficient fund');
                    return response()->json($res);
                    die;
                }
                
                $this->debitHelper($verify,'2500');
                $this->transactionHelper($verify,'2500','activation');
                    break;
                
                default:
                    # code...
                    break;
            }

           
            $device = new Devices;
            $device->user_id = $verify->id;
            $device->device_token = $request->device_token;
            $device->device_name = $request->device_name;
            $device->duration = $request->duration;
            $device->sim_port = $request->sim_port;
            $device->access_code = $this->generateKey(6);
            $device->save();

            
            return response()->json([
                'status' => 'success',
                'msg' => 'Created Successfully',
                'data' => $device
            ]);

        }

       
    } else {

        return response()->json([
            'status' => 'error',
            'msg' => 'Invaild Api Key'
           
        ]);

    }



   }



}
