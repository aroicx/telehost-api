<?php
namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Http\Controllers\Controller;
use App\Api\V1\Services\Dedicated;
use App\Api\V1\Services\Shared;
use Auth;
use App\User;
use App\Wallet;
use PayantNG\Payant;


class PaymentController extends Controller 
{

    public $pay;

    public function __construct()
    {
        $this->pay = new Payant\Payant('7b39e8e70fa07ea61cc0cd50a34c5707660ff38b8d7151f2af9a9950');
    }
    
    //invoice 
    public function invoice()
    {
        
        $client_id = '410';
        $client_data = [
            "first_name" => "Jonathan ",
            "last_name" => "Itakpe",
            "email" => "jonathan@floatr.com.ng",
            "phone" => "+234809012345"
        ];
        $due_date = "12/30/2016";
        $fee_bearer = "client";
        $items = [
              [
                "item" => ".Com Domain Name Registration",
                "description" => "alberthostpital.com",
                "unit_cost" => "2500.00",
                "quantity" => "1"
              ]
        ];

        $invoice =  $this->pay->addInvoice($client_id, $client_data, $due_date, $fee_bearer,$items);

    }

    public function getInvoice()
    {
      
        $result =  $this->pay->getInvoice();
        return $this->response->json($result);

    }

    public function addPay()
    {
      
        $reference_code = 'H5RJ2DGq6r3edZT0LKU8';

        if($reference_code != ''){
            $amount = "500.00";
            $channel = 'Cash';
           
                $add_payment =  $this->pay->addPayment($reference_code,$amount,$channel);
                return response()->json($add_payment);
            }else{
                die;
            }
     
    }


   public function getPayment()
   {
    $reference_code = 'H5RJ2DGq6r3edZT0LKU8';

    if($reference_code != ''){
            $get_payment =  $this->pay->getPayment($reference_code );
            return response()->json($get_payment);
        }else{
            die;
        }

    
   }



}
