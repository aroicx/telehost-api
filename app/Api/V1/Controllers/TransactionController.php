<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Transaction;


class TransactionController extends Controller
{
    public function index()
    {
        $trans = Transaction::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        if(count($trans) > 0){
            return response()->json(['status' => 'success','data' => $trans]);
        }else{
            return response()->json(['status' => 'success','data' => 'You have No Transactions Yet']);
        }
        

    }
}
