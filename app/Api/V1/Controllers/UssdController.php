<?php


namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Http\Controllers\Controller;
use App\Api\V1\Services\Dedicated;
use App\Api\V1\Services\Shared;
use Auth;
use App\User;
use App\Ussd;
use App\Wallet;
use App\Devices;
use App\ResquestTransaction;
use Carbon\Carbon;




class UssdController extends Controller
{
    
    public function findOne($ref)
    {
      $ussd = Ussd::where('ref_code', $ref)->first();
      if($ussd){
        return response()->json([
          'status' => 'success',
           'data' => $ussd
        ]);
      }else{
        return response()->json([
          'status' => 'error',
           'data' =>  'Not Found'
        ]);
      }
    }

    public function buy(Request $request)
    {

        echo $request->all();
        // $data = [
        //     "amount" => $request->amount,
        //     "service_category_id" => $request->service,
        //     "phonenumber" => $request->number
        // ];

        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "http://test-project.test/api/buy-airtime",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 30,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "POST",
        //   CURLOPT_POSTFIELDS => "{\n  \"amount\": $request->amount,\n  \"service\": $request->service,\n  \"number\": $request->number\n}",
        //   CURLOPT_HTTPHEADER => array(
        //     "Accept: */*",
        //     "Accept-Encoding: gzip, deflate",
        //     "Authorization: 4ee4d26de9dff84e5d7ec32e8dd54dc8722a7dcf7860795fa31021ba1006abe48e5a24a657f728616e3fd5a5d39324f65e889c35c401aec42e7c04ebd3ea9ab1",
        //     "Cache-Control: no-cache",
        //     "Connection: keep-alive",
        //     "Content-Length: 59",
        //     "Content-Type: application/json",
        //     "Host: test-project.test",
        //     "Postman-Token: 24062ce7-6362-422d-877f-0a809d85c2da,d0ff9f00-d961-45d5-ae6a-af2dc91bde0b",
        //     "User-Agent: PostmanRuntime/7.15.2",
        //     "cache-control: no-cache"
        //   ),
        // ));
        
        // $response = curl_exec($curl);
        // $err = curl_error($curl);
        
        // curl_close($curl);
        
        // if ($err) {
        //   echo "cURL Error #:" . $err;
        // } else {
        //   echo $response;
        // }
    }

    public function get()
    {
      $user = Auth::user();

      // $ussd = Ussd::where('user_id', $user->id)->orderBy('created_at', 'DESC')->get();
      $ussd = Ussd::where('user_id', $user->id)->orderBy('created_at', 'DESC')->paginate(10);

      return response()->json([
        'status' => 'success',
        'data' => $ussd
      ]);

      // echo $user;



    }

    public function  generateKey($length) {
        $key = '';
        $keys = array_merge(range(0, 9),range('a', 'z'));
        for ($i = 0; $i < $length; $i++) {
                $key .= $keys[array_rand($keys)];
            }
            return $key;
  }

    public function index(Request $request)
    {

      $headers = getallheaders();

      $verify = User::where('api_key',$headers['Authorization'])->first();
      if($verify){
        $request->validate([
          'ref_code' => 'required|unique:ussds'
      ]);
      $this->validateParameter('ref_code', $request->ref_code, STRING);
      $this->validateParameter('ussd_code', $request->ussd_code, STRING);
      $this->validateParameter('access_code', $request->access_code, STRING);

      $user = $verify->id;
      $getToken =  Devices::where('user_id',$user)->where('access_code', $request->access_code)->first();
      if(!$getToken){
        return response()->json([
          'status' => 'error',
          'msg' => 'Access Code error'
        
        ]);
      }
        if($getToken->status != 1){

          return response()->json([
            'status' =>  'error',
            'msg' => 'Current Devices activations is expired'
          ]);

        }else{
            $getUssd = Ussd::where('ref_code',$request->ref_code)->first();
            if(!$getUssd){
              $ussd = new Ussd;
              $ussd->user_id = $verify->id;
              $ussd->ref_code = $request->ref_code;
              $ussd->code_dialed = $request->ussd_code;
              $ussd->access_code = $request->access_code;
              $ussd->save();
            }
           

          
            $data = [  
              
                "to" => $getToken->device_token,
                "collapse_key" =>"aKey",
                "data" => [
                  "ref_code" => $request->ref_code,
                  "ussdString" => $request->ussd_code,
                  "sim" => $getToken->sim_port,
                ],
                "time_to_live" => 30,
              
            ];

          
            $toPost = json_encode($data);
        

            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $toPost,
              CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Authorization: key=AAAA0Ft6vlE:APA91bFATBXrgoQ3NinRAO8BNB9Cd3KODh42_lc0YhjOFUyN0MOvYs2KI7EUY2yA2p9dtTdtFK6wnp8B-X07HIaIOi92KxIENVBPIEweGdUkZthYZwA1UaLhPANZDIV0BuuCk9qmiJTw",
                "Content-Type: application/json"

              ),
            ));

              $response = curl_exec($curl);
              $err = curl_error($curl);

              curl_close($curl);

              if ($err) {
                // echo "cURL Error #:" . $err;
              } else {
                $character = json_decode($response);
                if ($character->success === 1) {
                  Ussd::where('ref_code' , $request->ref_code)->update(['status' => 1]);
                  sleep(10);
                  $success = ResquestTransaction::where('ref_code',$request->ref_code)->first();
                
                  if(!$success){
                    $this->createFailed($request);
                  }
                  return response()->json($response);
                }else{
                  return response()->json($response);
                }

                
              }


            } 
        }else {
          return response()->json([
            'status' => 'error',
            'msg' => 'Invaild Api Key'
          
          ]);
        }


    }
    public function tryAgain($data)
    {

      //  var_dump($data);
       $toPost = json_encode($data);
       $res = json_decode($toPost);
      //  return $res->access_code;

      $headers = getallheaders();

      $verify = User::where('api_key',$headers['Authorization'])->first();
      if($verify){
        
      $this->validateParameter('ref_code', $res->ref_code, STRING);
      $this->validateParameter('ussd_code', $res->ussd_code, STRING);
      $this->validateParameter('access_code', $res->access_code, STRING);

      $user = $verify->id;
      $getToken =  Devices::where('user_id',$user)->where('access_code', $res->access_code)->first();
      if(!$getToken){
        return response()->json([
          'status' => 'error',
          'msg' => 'Access Code error'
        
        ]);
      }
        if($getToken->status != 1){

          return response()->json([
            'status' =>  'error',
            'msg' => 'Current Devices activations is expired'
          ]);

        }else{
         
           

          
            $dataNew = [  
              
                "to" => $getToken->device_token,
                "collapse_key" =>"aKey",
                "data" => [
                  "ref_code" => $res->ref_code,
                  "ussdString" => $res->ussd_code,
                  "sim" => $getToken->sim_port,
                ],
                "time_to_live" => 30,
              
            ];

          
            $toPost = json_encode($dataNew);
        

            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $toPost,
              CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Authorization: key=AAAA0Ft6vlE:APA91bFATBXrgoQ3NinRAO8BNB9Cd3KODh42_lc0YhjOFUyN0MOvYs2KI7EUY2yA2p9dtTdtFK6wnp8B-X07HIaIOi92KxIENVBPIEweGdUkZthYZwA1UaLhPANZDIV0BuuCk9qmiJTw",
                "Content-Type: application/json"

              ),
            ));

              $response = curl_exec($curl);
              $err = curl_error($curl);

              curl_close($curl);

              if ($err) {
                // echo "cURL Error #:" . $err;
              } else {
                $character = json_decode($response);
                if ($character->success === 1) {
                  Ussd::where('ref_code' , $data->ref_code)->update(['status' => 1]);
                
                 
                  return response()->json($response);
                }else{
                  return response()->json($response);
                }

                
              }


            } 
        }else {
          return response()->json([
            'status' => 'error',
            'msg' => 'Invaild Api Key'
          
          ]);
        }


    }

    public function createFailed(Request $request)
    {
      

      $headers = getallheaders();
      $verify = User::where('api_key',$headers['Authorization'])->first();
      if($verify){

        $rt = new ResquestTransaction;
        $rt->user_id = $verify->id;
        $rt->ref_code = $request->ref_code;
        $rt->message = 'Request Failed';
        $rt->save();

        // echo $verify->webhook;
        // if($verify->webhook != null || ''){
        //   $this->ussd_webhookFailed($request,$verify->webhook);
        //   return response()->json(['status' => 'success', 'msg' => 'sent to webhook' ]);
        // }



        return response()->json(['status' => 'success' ]);

      } else {
        return response()->json([
          'status' => 'error',
          'msg' => 'Invaild Api Key'
         
        ]);
      }
     
    }


    public function ussd_webhookFailed(Request $request,$url)
    {

      $data = array(
        'ref_code' => $request->ref_code,
        'ussd_code' => $request->ussd_code,
        'message' => 'Request Failed'
      );

      $toPost = json_encode($data);
      $curl = curl_init();


      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $toPost,
        CURLOPT_HTTPHEADER => array(
          "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vdGVzdC1wcm9qZWN0LnRlc3QvYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1NjQ2NTE1NTAsImV4cCI6MTU2NDY1NTE1MCwibmJmIjoxNTY0NjUxNTUwLCJqdGkiOiJYZ2wxWW9pWXBnU1hnYVgwIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.F_HgymVcC_bJyNjLyt-CT-o9_-gD28fyNNZnu73rdo4",
          "Content-Type: application/json",
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        // echo $response;
      }



    }


    public function create(Request $request)
    {
      $headers = getallheaders();
      $this->validateParameter('ussd_code', $request->ussd_code, STRING);
      $this->validateParameter('ref_code', $request->ref_code, STRING);
      $this->validateParameter('message', $request->message, STRING);

      $verify = User::where('api_key',$headers['Authorization'])->first();
      if($verify){
        // sleep(5);
        $refCode = strtolower($request->ref_code);
        $success = ResquestTransaction::where('ref_code',$refCode)->first();

        $msg = $request->message;

        if($msg === 'Sorry, the process failed' ){
            $getRef = Ussd::where('ref_code',$request->ref_code)->first();

            $data = array(
              'ref_code' => $request->ref_code,
              'ussd_code' => $request->ussd_code,
              'access_code' => $getRef->access_code,
            );
            

          $this->tryAgain($data);
          die;
        }

        $haystack = 'how are you';
        $check = 'Failed to run';



        if(strpos($msg,$check) !== false){
            $getRef = Ussd::where('ref_code',$request->ref_code)->first();

            $data = array(
              'ref_code' => $request->ref_code,
              'ussd_code' => $request->ussd_code,
              'access_code' => $getRef->access_code,
            );
            

          $this->tryAgain($data);
          die;
        }
                
        if($success){

          ResquestTransaction::where('ref_code',$request->ref_code)
                  ->update(['message' => $request->message]);
        }else{

          $rt = new ResquestTransaction;
          $rt->user_id = $verify->id;
          $rt->ref_code = $request->ref_code;
          $rt->message = $request->message;
          $rt->save();
  
        }

       
        // echo $verify->webhook;
        if($verify->webhook != null || ''){
          $this->ussd_webhook($request,$verify->webhook);
          return response()->json(['status' => 'success', 'msg' => 'sent to webhook' ]);
        }



        return response()->json(['status' => 'success' ]);

      } else {
        return response()->json([
          'status' => 'error',
          'msg' => 'Invaild Api Key'
         
        ]);
      }
     
    }



    public function ussd_webhook(Request $request,$url)
    {

      $data = $request->all();
      $toPost = json_encode($data);
      $curl = curl_init();

   
      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $toPost,
        CURLOPT_HTTPHEADER => array(
          "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vdGVzdC1wcm9qZWN0LnRlc3QvYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1NjQ2NTE1NTAsImV4cCI6MTU2NDY1NTE1MCwibmJmIjoxNTY0NjUxNTUwLCJqdGkiOiJYZ2wxWW9pWXBnU1hnYVgwIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.F_HgymVcC_bJyNjLyt-CT-o9_-gD28fyNNZnu73rdo4",
          "Content-Type: application/json",
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        // echo $response;
      }



    }

    public function deleteOldUssd()
    {
      $ussd = Ussd::all();

      $expired = [];

      foreach ($ussd as $key => $value) {
       $date = Carbon::parse($value->updated_at);
       $now = Carbon::now();
       $diff = $date->diffInDays($now);
       $failed = ['user' => $value->user_id, 'duration' => $value->duration, 'expired_time' => $diff];

       array_push($expired,$failed);
       // sleep(6);
      }


      $proven = [];

      foreach ($expired as $key => $value) {
       // echo $value['duration'];

         if($value['expired_time'] > $value['duration']){
           $proof = ['user' => $value['user'], 'expired' => true];
         }else{
           $proof = ['user' => $value['user'], 'expired' => false];
         }
         array_push($proven,$proof);

      }

      foreach ($proven as $key => $value) {

      
           if($value['expired']){
                Devices::where('user_id', $value['user'])->update(['status' => 0]);
               // echo $value['user'];
           }
      }
    }

    public function retry(Request $request,$ref)
    {


      $getDetails = ResquestTransaction::where('ref_code',$ref)->first();
      $getUssd = Ussd::where('ref_code',$ref)->first();

      // return $getUssd;

      

      if(!$getDetails){
        return response()->json([
          'status' => 'error',
          'msg' => 'Ref Code error'
        
        ]);
      }


      $headers = getallheaders();

     
      $getToken =  Devices::where('access_code', $getUssd->access_code)->first();

      // return $getToken;
      if(!$getToken){
        return response()->json([
          'status' => 'error',
          'msg' => 'Access Code error'
        
        ]);
      }
        if($getToken->status != 1){

          return response()->json([
            'status' =>  'error',
            'msg' => 'Current Devices activations is expired'
          ]);

        }else{
          
            $data = [  
              
                "to" => $getToken->device_token,
                "collapse_key" =>"aKey",
                "data" => [
                  "ref_code" => $getDetails->ref_code,
                  "ussdString" => $getUssd->code_dialed,
                  "sim" => $getToken->sim_port,
                ],
                "time_to_live" => 30,
              
            ];

          
            $toPost = json_encode($data);

            // return $toPost;
        

            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $toPost,
              CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Authorization: key=AAAA0Ft6vlE:APA91bFATBXrgoQ3NinRAO8BNB9Cd3KODh42_lc0YhjOFUyN0MOvYs2KI7EUY2yA2p9dtTdtFK6wnp8B-X07HIaIOi92KxIENVBPIEweGdUkZthYZwA1UaLhPANZDIV0BuuCk9qmiJTw",
                "Content-Type: application/json"

              ),
            ));

              $response = curl_exec($curl);
              $err = curl_error($curl);

              curl_close($curl);

              if ($err) {
                // echo "cURL Error #:" . $err;
              } else {
                $character = json_decode($response);
                if ($character->success === 1) {
                 
                  return response()->json($response);
                }else{
                  return response()->json($response);
                }

                
              }


            } 
        

    }



}


