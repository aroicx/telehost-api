<?php

namespace App\Api\V1\Controllers;


use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Http\Controllers\Controller;
use App\Api\V1\Services\Dedicated;
use App\Api\V1\Services\Shared;
use Auth;
use App\User;
use App\Message;
use App\Wallet;
use App\Devices;
use App\ResquestTransaction;


class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get()
    {
      $user = Auth::user();

      $msg = Message::where('user_id', $user->id)->orderBy('created_at', 'DESC')->get();

      return response()->json([
        'status' => 'success',
        'data' => $msg
      ]);

      // echo $user;



    }
    public function index(Request $request)
    {

        $headers = getallheaders();

        $verify = User::where('api_key',$headers['Authorization'])->first();
            if($verify){
                $this->validateParameter('ref_code', $request->ref_code, STRING);
                $this->validateParameter('text', $request->text, STRING);
                $this->validateParameter('phone_number', $request->phone_number, STRING);
                // $this->validateParameter('sim', $request->sim, INTEGER);
                $this->validateParameter('access_code', $request->access_code, STRING);

                $user = $verify->id;
                $getToken =  Devices::where('user_id',$user)->where('access_code', $request->access_code)->first();
                if(!$getToken){
                    return response()->json([
                      'status' => 'error',
                      'msg' => 'Access Code error'
                    
                    ]);
                  }
                if($getToken->status != 1){
        
                    return response()->json([
                    'status' =>  'error',
                    'msg' => 'Current Devices activations is expired'
                    ]);
        
                }else{
                    $msg = new Message;
                    $msg->user_id = $verify->id;
                    $msg->ref_code = $request->ref_code;
                    $msg->reciever = $request->phone_number;
                    $msg->message = $request->text;
                    $msg->save();

                    $data = [  
                        "to" => $getToken->device_token,
                        "collapse_key" =>"aKey",
                        "data" => [
                            "body" => "The Body",
                            "title" => "Telehost",
                            "sim" => $getToken->sim_port,
                            "phone" => $request->phone_number,
                            "text" => $request->text
                        ]
                        
                    ];

                

                    
                    $toPost = json_encode($data);
                    

                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => $toPost,
                            CURLOPT_HTTPHEADER => array(
                                "Authorization: key=AAAA0Ft6vlE:APA91bFATBXrgoQ3NinRAO8BNB9Cd3KODh42_lc0YhjOFUyN0MOvYs2KI7EUY2yA2p9dtTdtFK6wnp8B-X07HIaIOi92KxIENVBPIEweGdUkZthYZwA1UaLhPANZDIV0BuuCk9qmiJTw",
                                "Content-Type: application/json",
                            ),
                        ));

                        $response = curl_exec($curl);
                        $err = curl_error($curl);

                        curl_close($curl);

                        if ($err) {
                            echo "cURL Error #:" . $err;
                            } else {
                            
                                $character = json_decode($response);
                                if ($character->success === 1) {
                                
                                    Message::where('ref_code' , $request->ref_code)->update(['status' => 1]);
                                    return response()->json($response);
                                }else{
                                    return response()->json($response);   
                                }
                        }
                    }

                }else{
                    return response()->json([
                        'status' => 'error',
                        'msg' => 'Invaild Api Key'
                        
                    ]);
            }



            /*....*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $headers = getallheaders();
        $this->validateParameter('ref_code', $request->ref_code, STRING);
        $this->validateParameter('message', $request->message, STRING);

        $verify = User::where('api_key',$headers['Authorization'])->first();
        if($verify){

            $rt = new ResquestTransaction;
            $rt->user_id = $verify->id;
            $rt->ref_code = $request->ref_code;
            $rt->message = $request->message;
            $rt->save();

            if($verify->webhook != null || ''){
                $this->sms_webhook($request,$verify->webhook);
              }


            return response()->json(['status' => 'success' ]);

        }else{
            return response()->json([
                'status' => 'error',
                'msg' => 'Invaild Api Key'
               
              ]);
        }
    }


    public function sms_webhook(Request $request,$url)
    {
    


      $data = $request->all();
      $toPost = json_encode($data);
      $curl = curl_init();


      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $toPost,
        CURLOPT_HTTPHEADER => array(
          "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vdGVzdC1wcm9qZWN0LnRlc3QvYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1NjQ2NTE1NTAsImV4cCI6MTU2NDY1NTE1MCwibmJmIjoxNTY0NjUxNTUwLCJqdGkiOiJYZ2wxWW9pWXBnU1hnYVgwIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.F_HgymVcC_bJyNjLyt-CT-o9_-gD28fyNNZnu73rdo4",
          "Content-Type: application/json",
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        // echo $response;
      }



    }
    /**
     * multi a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function multi(Request $request)
    {
        $headers = getallheaders();

        $verify = User::where('api_key',$headers['Authorization'])->first();
            if($verify){
                $this->validateParameter('ref_code', $request->ref_code, STRING);
                $this->validateParameter('text', $request->text, STRING);
                // $this->validateParameter('phone_number', $request->phone_number, STRING);
                // $this->validateParameter('sim', $request->sim, INTEGER);
                $this->validateParameter('access_code', $request->access_code, STRING);

                $user = $verify->id;
                $getToken =  Devices::where('user_id',$user)->where('access_code', $request->access_code)->first();
                
                // $msg = new Message;
                // $msg->user_id = $verify->id;
                // $msg->ref_code = $request->ref_code;
                // $msg->reciever = $request->phone_number;
                // $msg->message = $request->text;
                // $msg->save();

                $arr = $request->phone_number;
                // $data  = implode(" ",$arr);
                
                foreach ($arr as $key => $value) {
                    

                    $data = [  
                        "to" => $getToken->device_token,
                        "collapse_key" =>"aKey",
                        "data" => [
                            "body" => "The Body",
                            "title" => "Telehost",
                            "sim" => $getToken->sim_port,
                            "phone" => $value,
                            "text" => $request->text
                        ]
                        
                    ];

                    $toPost = json_encode($data);
                    $this->FCMhelper($toPost);




                }

                }else{
                    return response()->json([
                        'status' => 'error',
                        'msg' => 'Invaild Api Key'
                        
                    ]);
            }





        
    }


    public function FCMhelper($data)
    {
        $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $data,
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: key=AAAA0Ft6vlE:APA91bFATBXrgoQ3NinRAO8BNB9Cd3KODh42_lc0YhjOFUyN0MOvYs2KI7EUY2yA2p9dtTdtFK6wnp8B-X07HIaIOi92KxIENVBPIEweGdUkZthYZwA1UaLhPANZDIV0BuuCk9qmiJTw",
                            "Content-Type: application/json",
                        ),
                    ));

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        echo "cURL Error #:" . $err;
                        } else {
                            // print_r($response);
                            $character = json_decode($response);
                            if ($character->success === 1) {
                            
                            // Message::where('ref_code' , $request->ref_code)->update(['status' => 1]);
                            //  print_r($response);
                            }
                    }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
