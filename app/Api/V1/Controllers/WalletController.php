<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Http\Controllers\Controller;
use App\Api\V1\Services\Dedicated;
use App\Api\V1\Services\Shared;
use Auth;
use Mail;
use App\User;
use App\Wallet;
use App\Sim;
use App\Transaction;
use App\ResquestTransaction;
use PayantNG\Payant;
use App\Mail\WalletDebited;



class WalletController extends Controller
{


    public function __construct(Request $request)
    {
        $this->middleware('jwt.auth', []);
        
    }
    
    public function index()
    {
      $user = Auth::User()->id;
        try {
            $wallet = Wallet::where('user_id',$user)->with('parent')->first();
            if($wallet){
                return response()->json([
                    'status' => 'success',
                    'data' => $wallet
                ]);
            }else{
                return response()->json(['status' => 'error','message' => 'Wallet Not Found']);
            }
            } catch (JWTException $e) {
                throw new HttpException(500);
            }
    }

    public function getResponse(){
        $user = Auth::user();

        // $res = ResquestTransaction::where('user_id', $user->id)->get();
        $res = ResquestTransaction::where('user_id', $user->id)->get();

        return response()->json([
            'status' => 'success',
            'data' => $res
        ]);

    }

    public function checkBalance(Request $request){

        $wal = Wallet::where('user_id', Auth::user()->id)->first();
        if($request->amount){
            if($wal->balance < $request->amount){
                $res = array('status' => 'error', 'message' => 'Insufficient fund');
                return response()->json($res);
                die;
            }
        }
        
    }

    public function transactionHelper($amount,$choice)
    {
        $choosen = null;
        switch ($choice) {
            case 'topup':
            $choosen = 'topup';
                break;
            case 'funding':
            $choosen = 'funding';
                break;
            
            case 'sms':
            $choosen = 'sms';
                break;
            
            case 'ussd':
            $choosen = 'ussd';
                break;

            case 'call':
            $choosen = 'call';
                break;

            case 'sim':
            $choosen = 'sim';
                break;
            
            default:
                # code...
                break;
        }

       try {
        $transaction = new Transaction;
        $transaction->user_id = Auth::user()->id;
        $transaction->transaction_type = $choosen;
        $transaction->amount = $amount;
        $transaction->save();
       } catch (JWTException $e) {
        throw new HttpException(500);
        }


    }

    public function debitHelper(Request $request)
    {
        
       try {
        $wal = Wallet::where('user_id', Auth::user()->id)->first();
        $per = ($request->amount * 3)/100;
        $total = $request->amount - $per;
        $debited = $wal->balance - $total;
        Wallet::where('user_id', Auth::User()->id)->update(['balance' => $debited]);
       } catch (JWTException $e) {
        throw new HttpException(500);
        }


    }

    public function funding(Request $request)
    {
        
        
        $Payant = new Payant\Payant('ce3b09565a2e97a45e8f86f1046204e8f537e417b6cfcc88ec015765');
        // $amount = $request->amount;
        $reference_code = $request->reference_code;

        if($reference_code != ''){
            $amount = $request->amount;
            $add_payment = $Payant->getPayment($reference_code);

            
            // return $add_payment->status;
            if($add_payment->status === 'success'){
                $wallet = Wallet::where('user_id', Auth::user()->id)->first();
                $wallet->update(['balance' => $wallet->balance+$amount]);
                $this->transactionHelper($amount,'funding');
                return response()->json(['status' => 'success','message' => 'Wallet has been founded with '.$amount]);
            }   
            
        }else{
            return response()->json(['status' => 'error','message' => 'Payment Failed']);
           
        }
     



    }

    public function buy_aritime(Request $request) {

        $wal = Wallet::where('user_id', Auth::user()->id)->first();
        if($request->amount){
            if($wal->balance < $request->amount){
                $res = array('status' => 'error', 'message' => 'Insufficient fund');
                return response()->json($res);
            }
        }

        $this->validateParameter('amount', $request->amount, STRING);
        $this->validateParameter('network', $request->network, STRING);
        $this->validateParameter('number', $request->number, STRING);

       
            $code = null;
            switch ($request->network) {
                case 'Airtel':
                    $code = 6;
                    break;
                case 'Mtn':
                    $code = 7;
                    break;
                case 'Glo':
                    $code = 8;
                    break;
                case '9mobile':
                    $code = 9;
                    break;
                
                default:
                    # code...
                    break;
            }
          

            $data = [
                "amount" => $request->amount,
                "service_category_id" => $code,
                "phonenumber" => $request->number
            ];

            
            
            $toPost = json_encode($data);
            
            // return $toPost;
            // die;
           $curl = curl_init();
           curl_setopt_array($curl, array(
           CURLOPT_URL => "https://api.myflex.ng/bills/pay/airtime",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => $toPost,
           CURLOPT_HTTPHEADER => array(
               "Accept: */*",
               "Authorization: 12632008c3b59e037f2e761675c88296e146add6240cb9d35ec6366041238417ea833675ce168e34a852457224a827a0ff2e723a95ae682458c6ef4baee2118c71",
               "Content-Type: application/json"
           
           ),
           ));

           $response = curl_exec($curl);
          
           $err = curl_error($curl);

           curl_close($curl);

           if ($err) {
            // echo "cURL Error #:" . $err;
            } else {
                
                $character = json_decode($response);
                if ($character->status === 'success') {
                // create new transaction
                  $this->transactionHelper($request->amount,'topup');      
                //dedit wallet
                $this->debitHelper($request);          
                return response()->json(['status' => 'success','message' => 'Successful']);
                }else{
                    return response()->json($response);
                }
           }
       
       
    
    }

    public function buy_aritime_web(Request $request) {

        //create new transaction
        $this->transactionHelper($request->amount,'topup');      
        //dedit wallet
        $this->debitHelper($request);      
        try {
        $user = Auth::user();
        $wallet = Wallet::where('user_id', $user->id)->first();
    
        // Mail::to($user->email)->send(new WalletDebited($user,$request->amount,'Airtime Purchase'));
        } catch (JWTException $e) {
            throw new HttpException(500);
        }
        
        return response()->json([
            'status' => 'success',
            'message' => 'Successful',
            'wallet_balance' => $wallet->balance
        ]);
    
          
       
       
    
    }
    
    public function buy_sim(Request $request) {

        
        $wal = Wallet::where('user_id', Auth::user()->id)->first();
        if($request->amount){
            if($wal->balance < $request->amount){
                $res = array('status' => 'error', 'message' => 'Insufficient fund');
                return response()->json($res);
                die;
            }
        }
       

        $network = $request->network;
        $number = $request->number;
        $type = $request->type;

        $buy = Sim::where('type',$type)
                ->where('network',$network)
                ->where('number',$number)
                ->where('assigned', 0)
                ->first();
        if($buy){
            Sim::where('type',$type)
            ->where('network',$network)
            ->where('number',$number)
            ->update(['assigned' => 1]);

            $this->transactionHelper($request->amount,'sim');      
            $this->debitHelper($request);      
            try {
                $user = Auth::user();   
            // Mail::to($user->email)->send(new WalletDebited($user,$request->amount,'Sim Purchase'));
            } catch (JWTException $e) {
                throw new HttpException(500);
            }

            return response()->json(['status' => 'success','message' => 'Purchase Successful']);
        }else{
            die;
        }





        return $buy;
           
               
           
       
       
    
    }




   


}
