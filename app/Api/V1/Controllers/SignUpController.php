<?php

namespace App\Api\V1\Controllers;

use Config;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\User;
use Mail;
use App\Mail\UserActivation;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {

        $data = array(
            'api_key' => $this->generateKey(250),
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'phone_num' => $request->phone_num,
            'country' => $request->country,
            'password' => $request->password
        );


        // echo json_encode($data);

        $user = new User($data);
       
       
        if(!$user->save()) {
            throw new HttpException(500);
        }
        try {
                
            // Mail::to($request->email)->send(new UserActivation($user));
        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        if(!Config::get('boilerplate.sign_up.release_token')) {

            
            
            return response()->json([
                'status' => 'ok',
                'user' => $user
            ], 201);
        }

        

        $token = $JWTAuth->fromUser($user);
        return response()->json([
            'status' => 'ok',
            'token' => $token
        ], 201);
    }
    public function  generateKey($length) 
    {
       $key = '';
       $keys = array_merge(range(0, 9),range('a', 'z'));
       for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
        
     }


     public function getCountries()
     {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://country.io/names.json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET"
        ));

        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {

            $all = [];
            foreach ($response as $key => $value) {
              array_push($all,$value);
            }

            return response()->json([
                'status' => 'success',
                'data' => $all
            ]);
            
        }
                


     }
}
