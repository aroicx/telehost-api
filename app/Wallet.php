<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;


class Wallet extends Model
{

    protected $fillable = [
        'user_id','balance'
    ];






    public function parent() {
        return $this->belongsTo(User::class);
    }
}
