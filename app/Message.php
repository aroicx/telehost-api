<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Message extends Model
{
    public function parent() {
        return $this->belongsTo(User::class);
    }

}
