<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Devices extends Model
{
    protected $fillable = [
        'user_id','device_token','number'
    ];

    public function parent() {
        return $this->belongsTo(User::class);
    }
}
