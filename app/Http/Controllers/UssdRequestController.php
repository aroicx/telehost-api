<?php

namespace App\Http\Controllers;

use App\UssdRequest;
use Illuminate\Http\Request;

class UssdRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UssdRequest  $ussdRequest
     * @return \Illuminate\Http\Response
     */
    public function show(UssdRequest $ussdRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UssdRequest  $ussdRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(UssdRequest $ussdRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UssdRequest  $ussdRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UssdRequest $ussdRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UssdRequest  $ussdRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(UssdRequest $ussdRequest)
    {
        //
    }
}
