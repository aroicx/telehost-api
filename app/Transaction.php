<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Transaction extends Model
{
    protected $fillable = [
        'user_id','amount','transaction_type','status'
    ];

    public function parent() {
        return $this->belongsTo(User::class);
    }
}
