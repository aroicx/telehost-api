@component('mail::message')
# Welcome {{$user->name}} to TeleHost


Thanks for signing up. **We really apperciate** it. 
Please Kindly follow the link below to activate your account.

The body of your message.


<style>
    .btn{
        text-decoration: none;
        min-width: 120px;
        height: 50px;
        padding: 15px 12px;
        border-radius: 3px;
        background: #3498db;
        color: #fff !important;
        margin: 50px 100px;
    }
</style>

<a class="btn" href="http://test-project.test/api/verify/{{$user->api_key}}" >
    Verify Account
</a>




Thanks,<br>
{{ config('app.name') }}
@endcomponent
