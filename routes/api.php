<?php

use Dingo\Api\Routing\Router;
use Illuminate\Http\Request;


/** @var Router $api */
$api = app(Router::class);

    $api->version('v1', function (Router $api) {
        $api->get('/', function () { return response()->json(['status' => '200', 'msg' => 'success']);
    });
    $api->group(['prefix' => 'auth'], function(Router $api) {

       
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login-web', 'App\\Api\\V1\\Controllers\\LoginWebController@login');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');
        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('profile', 'App\\Api\\V1\\Controllers\\UserController@me');
        $api->get('check', 'App\\Api\\V1\\Controllers\\UserController@check');
        $api->post('change-paasword', 'App\\Api\\V1\\Controllers\\UserController@updatePassword');
        $api->post('addWebhook', 'App\\Api\\V1\\Controllers\\UserController@addWebhook');     
        $api->get('countries', 'App\\Api\\V1\\Controllers\\SignUpController@getCountries');
    });

    

    $api->group(['prefix' => 'admin','middleware' => 'admin'], function(Router $api) {
        $api->get('schedule', 'App\\Api\\V1\\Controllers\\AdminController@schedule');
        $api->get('users', 'App\\Api\\V1\\Controllers\\AdminController@getUsers');
        $api->get('user/{id}', 'App\\Api\\V1\\Controllers\\AdminController@getUser');
        $api->get('wallets', 'App\\Api\\V1\\Controllers\\AdminController@getWallets');
        
    });
    $api->get('duration', 'App\\Api\\V1\\Controllers\\AdminController@DurationCron');

    $api->group(['middleware' => 'protected.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);


        $api->post('add-invoice', 'App\\Api\\V1\\Controllers\\PaymentController@invoice');
        //wallets
        $api->post('fund', 'App\\Api\\V1\\Controllers\\WalletController@funding');
        $api->get('wallet', 'App\\Api\\V1\\Controllers\\WalletController@index');
        // user verification
        $api->get('verify/{token}', 'App\\Api\\V1\\Controllers\\UserController@verify');
        $api->get('mail', 'App\\Api\\V1\\Controllers\\UserController@mail');
        // buy airtime
        $api->post('buy-airtime', 'App\\Api\\V1\\Controllers\\WalletController@buy_aritime_web');
        $api->post('buy-airtime-app', 'App\\Api\\V1\\Controllers\\WalletController@buy_aritime');
        //buy sim
        $api->post('buy-sim', 'App\\Api\\V1\\Controllers\\WalletController@buy_sim'); 
        //get transaction
        $api->get('my-transactions', 'App\\Api\\V1\\Controllers\\TransactionController@index');
        //get sims
        $api->get('get-sims', 'App\\Api\\V1\\Controllers\\SimController@all');
        // get ussd sent
        $api->get('get-ussd', 'App\\Api\\V1\\Controllers\\UssdController@get');
        // get msg sent
        $api->get('get-message', 'App\\Api\\V1\\Controllers\\SmsController@get');
        $api->get('get-response', 'App\\Api\\V1\\Controllers\\WalletController@getResponse');
        
        $api->get('get-devices', 'App\\Api\\V1\\Controllers\\DevicesController@index');    
        $api->get('dashboard', 'App\\Api\\V1\\Controllers\\UserController@dashboard');    


        //activate device
        
    });

    $api->post('activate-device', 'App\\Api\\V1\\Controllers\\DevicesController@activate');    
    //hoook
    $api->post('ussd-webhook', 'App\\Api\\V1\\Controllers\\UssdController@ussd_webhook'); 
    //send ussd
    $api->get('ussd/{ref}', 'App\\Api\\V1\\Controllers\\UssdController@findOne');
    $api->post('post-ussd', 'App\\Api\\V1\\Controllers\\UssdController@index');
    $api->post('send-ussd-response', 'App\\Api\\V1\\Controllers\\UssdController@create');
    $api->get('retry-ussd/{ref}', 'App\\Api\\V1\\Controllers\\UssdController@retry');
    //sms     
    $api->post('post-sms', 'App\\Api\\V1\\Controllers\\SmsController@index');
    $api->post('post-multisms', 'App\\Api\\V1\\Controllers\\SmsController@multi');
    $api->post('send-sms-response', 'App\\Api\\V1\\Controllers\\SmsController@create');
    

    $api->get('get-data/{network}', 'App\\Api\\V1\\Controllers\\DataController@getNetwork');
    $api->post('buy-data', 'App\\Api\\V1\\Controllers\\DataController@buyData');
    $api->post('send-data-response', 'App\\Api\\V1\\Controllers\\DataController@create');



    $api->get('get-networks', function(){
   
        return response()->json([
            'status' => 'success',
            'data' => [
                [
                    'network' => 'Airtel',
                    'service_number' => 6
                ],
                [
                    'network' => 'Mtn',
                    'service_number' => 7
                ],
                [
                    'network' => 'Glo',
                    'service_number' => 8
                ],
                [
                    'network' => '9mobile',
                    'service_number' => 9
                ],
            ] 
        ]);

    });


    
});
